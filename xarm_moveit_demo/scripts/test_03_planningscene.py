#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, sys, os
import moveit_commander
from moveit_commander import MoveGroupCommander, PlanningSceneInterface
from geometry_msgs.msg import PoseStamped, Pose
from moveit_msgs.msg import PlanningScene, ObjectColor
from math import pi
from copy import deepcopy
import tf
from tf.transformations import *


class MoveItObstaclesDemo:
    def __init__(self):

        # 初始化Python API 依赖的moveit_commanderC++系统，需放在前面
        moveit_commander.roscpp_initialize(sys.argv)

        # 初始化ROS节点，节点名为'moveit_obstacles_demo'
        rospy.init_node('moveit_obstacles_demo', anonymous=True)

        # 初始化场景对象
        scene = PlanningSceneInterface()
        rospy.sleep(1)

        # 初始化需要控制的规划组。手臂xarm，手爪gripper
        arm = moveit_commander.MoveGroupCommander('xarm')
        gripper = moveit_commander.MoveGroupCommander('gripper')

        # 设置位置(单位：米)和姿态（单位：弧度）的允许误差
        arm.set_goal_position_tolerance(0.02)
        arm.set_goal_orientation_tolerance(0.03)

        # 当运动规划失败后，允许重新规划
        arm.allow_replanning(True)

        # 设置目标位姿为Home，Home是setup assistant中预先定义好的初始位姿的名称。
        arm.set_named_target('Home')
        arm.go()

        print "============ Press `Enter` to add objects to the planning scene ..."
        raw_input()

        # 将桌子(长方体桌面)添加到规划场景中
        table_id = 'table'
        table_size = [1.0, 1.2, 0.01]
        table_pose = PoseStamped()
        table_pose.header.frame_id = 'base_link'
        table_pose.pose.position.x = 0.0
        table_pose.pose.position.y = 0.0
        table_pose.pose.position.z =  -table_size[2]/2
        table_pose.pose.orientation.w = 1.0
        scene.add_box(table_id, table_pose, table_size)
        if self.wait_for_state_update(table_id,scene,obstacle_is_known=True):
            rospy.loginfo("The table has been successfully added.")
        else:
            rospy.loginfo("Failed to add the table.")

        # 将一个长方体box1添加到规划场景中
        # 设置长方体box1的ID、尺寸和位姿
        box1_id = 'box1'
        box1_size = [0.3, 0.3, 0.04]
        box1_pose = PoseStamped()
        box1_pose.header.frame_id = 'base_link'
        box1_pose.pose.position.x = 0.4
        box1_pose.pose.position.y = 0
        box1_pose.pose.position.z = box1_size[2]/2 + 0.37
        box1_pose.pose.orientation.w = 1.0

        # TODO：调用add_box()函数将box1添加到规划场景中



        # 判断是否已经将box1添加到规划场景中
        if self.wait_for_state_update(box1_id,scene,obstacle_is_known=True):
            rospy.loginfo("The box1 has been successfully added.")
        else:
            rospy.loginfo("Failed to add the box1.")


        # 将一个小长方体box2添加到规划场景中
        box2_id = 'box2'
        box2_size = [0.02, 0.02, 0.12]
        box2_pose = PoseStamped()
        box2_pose.header.frame_id = 'base_link'
        box2_pose.pose.position.x = 0.4
        box2_pose.pose.position.y = 0
        box2_pose.pose.position.z = box2_size[2]/2 + box1_size[2]/2 + 0.37
        box2_pose.pose.orientation.w = 1.0
        scene.add_box(box2_id, box2_pose, box2_size)
        if self.wait_for_state_update(box2_id,scene,obstacle_is_known=True):
            rospy.loginfo("The box has been successfully added.")
        else:
            rospy.loginfo("Failed to add the box2.")

        print " "
        print "============ Press `Enter` to open gripper, move the arm and close gripper..."
        raw_input()

        # 设置手爪规划组gripper里两个关节的位置为0.65，单位弧度
        joint_positions = [0.65, 0.65]
        gripper.set_joint_value_target(joint_positions)
        # 规划并执行，手爪张开
        gripper.go()
        rospy.sleep(1)

        # 设置机械臂末端执行器的目标位姿，参考坐标系为base_link
        # 位置通过xyz设置，姿态用四元数表示
        target_pose = PoseStamped()
        target_pose.header.frame_id = "base_link"
        target_pose.header.stamp = rospy.Time.now()
        target_pose.pose.position.x = 0.41
        target_pose.pose.position.y = 0
        target_pose.pose.position.z = box2_size[2]/2 + box1_size[2]/2 + 0.37
        target_pose.pose.orientation.w = 1

        # 设置机器臂当前的状态作为运动初始状态
        arm.set_start_state_to_current_state()
        # 设置目标target_pose并让机械臂运动到此目标
        end_effector_link = arm.get_end_effector_link()
        arm.set_pose_target(target_pose, end_effector_link)
        arm.go()
        rospy.sleep(1)

        # 闭合手爪
        joint_positions = [0.2, 0.2]
        gripper.set_joint_value_target(joint_positions)
        gripper.go()
        rospy.sleep(1)

        print " "
        print "============ Press `Enter` to attach the box2 to the XBot-Arm robot ..."
        raw_input()


        # TODO: 使用把attach_box()函数把box2附着到机械臂末端执行器上


        # 判断box2是否附着成功
        if self.wait_for_state_update(box2_id,scene,box_is_attached=True):
            rospy.loginfo("The box2 has been successfully attached.")
        else:
            rospy.loginfo("Failed to attach the box2l.")

        print " "
        print "============ Press `Enter` to move the arm to a position 27 cm down  ..."
        raw_input()
        # 使用shift_pose_target()函数设置目标位姿为当前位置的下方27厘米处，也就是沿着Z轴下移0.27m的位置
        arm.shift_pose_target(2,-0.27,end_effector_link)
        arm.go()
        rospy.sleep(1)

        print " "
        print "============ Press `Enter` to detach the box2 from the XBot-Arm robot ..."
        raw_input()

        # TODO：使用remove_attached_object()函数将box2从机械臂上分离


        rospy.sleep(1)

        print " "
        print "============ Press `Enter` to remove the obstacles ..."
        raw_input()
        # 从规划场景里移除桌子
        scene.remove_world_object(table_id)
        # 从规划场景里移除box2
        scene.remove_world_object(box2_id)

        # TODO：从规划场景里移除box1


        rospy.sleep(1)

        print " "
        print "============ Press `Enter` to move to Home and exit the program ..."
        raw_input()
        arm.set_named_target('Home')
        arm.go()
        rospy.sleep(1)
        # 完全闭合手爪
        joint_positions = [0.0, 0.0]
        gripper.set_joint_value_target(joint_positions)
        gripper.go()
        rospy.sleep(1)

        # 干净地关闭moveit_commander并退出程序
        moveit_commander.roscpp_shutdown()
        moveit_commander.os._exit(0)

    # 判断是否已成功添加物体到规划场景，或是否成功把物体附着到机械臂上
    def wait_for_state_update(self,obstacle_name,scene,obstacle_is_known=False, box_is_attached=False, timeout=4):
        start = rospy.get_time()
        seconds = rospy.get_time()
        # 在4s的时间内，循环判断我们想要的规划场景状态是否更新成功，若成功，返回True
        while (seconds - start < timeout) and not rospy.is_shutdown():
          # 判断物体是否attach成功
          attached_objects = scene.get_attached_objects([obstacle_name])
          is_attached = len(attached_objects.keys()) > 0

          # 判断传入的物体是否已经在规划场景中，即物体是否添加成功
          is_known = obstacle_name in scene.get_known_object_names()

          # 判断是否是我们想要的规划场景更新状态
          if (box_is_attached == is_attached) and (obstacle_is_known == is_known):
            return True

          # sleep0.1s，为物体添加或附着留出时间。
          rospy.sleep(0.1)
          seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False


if __name__ == "__main__":
    try:
        MoveItObstaclesDemo()
    except rospy.ROSInterruptException:
        pass
