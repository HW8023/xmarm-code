#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, sys
import moveit_commander
import math
from geometry_msgs.msg import PoseStamped, Pose
from moveit_commander import MoveGroupCommander, PlanningSceneInterface
from moveit_msgs.msg import PlanningScene, ObjectColor
from moveit_msgs.msg import Grasp,PlaceLocation,GripperTranslation, MoveItErrorCodes
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from tf.transformations import quaternion_from_euler
from copy import deepcopy

GROUP_NAME_ARM = 'xarm'
GROUP_NAME_GRIPPER = 'gripper'
GRIPPER_FRAME = 'gripper_centor_link'
GRIPPER_OPEN = [0.65,0.65]
# 根据抓取对象的尺寸设置抓取位置
GRIPPER_GRASP = [0.2,0.2]
GRIPPER_CLOSED = [0.0,0.0]
GRIPPER_JOINT_NAMES = ['gripper_1_joint','gripper_2_joint']
GRIPPER_EFFORT = [0,0]
REFERENCE_FRAME = 'base_link'

class MoveItPickPlaceDemo:
    def __init__(self):
        # 初始化Python API 依赖的moveit_commanderC++系统
        moveit_commander.roscpp_initialize(sys.argv)
        # 初始化节点
        rospy.init_node('test_04_pick_place')

        # 初始化场景对象，用来在规划场景中添加或移除物体
        scene = PlanningSceneInterface()
        # Give the scene a chance to catch up
        rospy.sleep(1)

        # 创建一个话题发布端用来将抓取位姿发布到gripper_pose上，在RViz中可视化
        self.gripper_pose_pub = rospy.Publisher('gripper_pose', PoseStamped,queue_size=10)

        # Initialize the move group for the xarm
        xarm = MoveGroupCommander(GROUP_NAME_ARM)

        # Initialize the move group for the gripper
        gripper = MoveGroupCommander(GROUP_NAME_GRIPPER)

        # Get the name of the end-effector link
        end_effector_link = xarm.get_end_effector_link()

        # Allow replanning to increase the odds of a solution
        xarm.allow_replanning(True)

        # Set the right arm reference frame
        xarm.set_pose_reference_frame(REFERENCE_FRAME)

        # Allow 5 seconds per planning attempt
        xarm.set_planning_time(5)

        # Set a limit on the number of pick attempts before bailing
        max_pick_attempts = 5

        # Set a limit on the number of place attempts
        max_place_attempts = 5

        print "============ Press `Enter` to start the pick place demo ..."
        raw_input()

        # Give each of the scene objects a unique name
        table_id = 'table'
        target_id = 'box'

        # Remove leftover objects from a previous run
        scene.remove_world_object(table_id)
        scene.remove_world_object(target_id)

        # Remove any attached objects from a previous session
        scene.remove_attached_object(GRIPPER_FRAME, target_id)

        # Give the scene a chance to catch up
        rospy.sleep(1)

        # 机械臂先回到初始位置
        xarm.set_named_target('Home')
        xarm.go()
        rospy.sleep(1)

        # 设置桌子的长宽高 [l, w, h]
        table_size = [1.2, 1.4, 0.01]
        # 设置桌子的位姿
        table_pose = PoseStamped()
        table_pose.header.frame_id = REFERENCE_FRAME
        table_pose.pose.position.x = 0.0
        table_pose.pose.position.y = 0.0
        table_pose.pose.position.z = - table_size[2] / 2.0
        table_pose.pose.orientation.w = 1.0
        # 把桌子添加到规划场景中
        scene.add_box(table_id, table_pose, table_size)

        # 设置目标物体的长宽高 [l, w, h]
        target_size = [0.06, 0.06, 0.06]
        # 设置目标的位姿，让目标物体位于桌子上
        target_pose = PoseStamped()
        target_pose.header.frame_id = REFERENCE_FRAME
        target_pose.pose.position.x = 0.4
        target_pose.pose.position.y = 0.0
        target_pose.pose.position.z =  target_size[2] / 2.0
        target_pose.pose.orientation.w = 1.0

        # 把目标物体添加到规划场景中
        scene.add_box(target_id, target_pose, target_size)
        rospy.sleep(1)

        # TODO:使用set_support_surface_name()函数设置桌子table为抓取和放置操作的支撑面，
        # 使MoveIt!忽略物体放到桌子上时产生的碰撞警告



        # 初始化抓取的目标位姿
        grasp_pose = target_pose
        # 考虑到抓取方块时，希望手爪竖直向下，能夹住方块的两边，
        # 所以需要设置gripper_centor_link的位置位于方块中心上方，姿态为(0, math.pi/2, 0)
        grasp_pose.pose.position.z += 0.02
        q = quaternion_from_euler(0, math.pi/2, 0)
        grasp_pose.pose.orientation.x = q[0]
        grasp_pose.pose.orientation.y = q[1]
        grasp_pose.pose.orientation.z = q[2]
        grasp_pose.pose.orientation.w = q[3]

        # 生成grasp抓取列表
        grasps = self.make_grasps(grasp_pose, [table_id])
        print grasps
        # 发布grasp_pose，用以在RViz中查看
        for grasp in grasps:
            self.gripper_pose_pub.publish(grasp.grasp_pose)
            rospy.sleep(0.4)

        # 设置result标记每次抓取尝试的结果
        result = None
        n_attempts = 0

        # 循环尝试抓取操作直到抓取成功或最大尝试次数用尽
        while result != MoveItErrorCodes.SUCCESS and n_attempts < max_pick_attempts:
            n_attempts += 1
            rospy.loginfo("Pick attempt: " +  str(n_attempts))
            result = xarm.pick(target_id, grasps)
            rospy.sleep(0.2)

        # 如果抓取成功，尝试物品放置操作
        if result == MoveItErrorCodes.SUCCESS:
            result = None
            n_attempts = 0

            # 设置一个放置目标位姿place_pose
            place_pose = PoseStamped()
            place_pose.header.frame_id = REFERENCE_FRAME
            place_pose.pose.position.x = 0.3
            place_pose.pose.position.y = 0
            place_pose.pose.position.z = target_size[2] / 2.0
            place_pose.pose.orientation.w = 1

            # 生成一系列放置位姿
            places = self.make_places(place_pose)

            # 循环放置直到放置成功或超过最大尝试次数
            while result != MoveItErrorCodes.SUCCESS and n_attempts < max_place_attempts:
                n_attempts += 1
                rospy.loginfo("Place attempt: " +  str(n_attempts))
                for place in places:
                    result = xarm.place(target_id, place)
                    if result == MoveItErrorCodes.SUCCESS:
                        break
                rospy.sleep(0.2)

            if result != MoveItErrorCodes.SUCCESS:
                rospy.loginfo("Place operation failed after " + str(n_attempts) + " attempts.")
        else:
            rospy.loginfo("Pick operation failed after " + str(n_attempts) + " attempts.")

        # 回到初始位姿
        xarm.set_named_target('Home')
        xarm.go()
        rospy.sleep(1)
        # 闭合手爪
        gripper.set_joint_value_target(GRIPPER_CLOSED)
        gripper.go()
        rospy.sleep(1)

        # Shut down MoveIt cleanly
        moveit_commander.roscpp_shutdown()

        # Exit the script
        moveit_commander.os._exit(0)

    # Get the gripper posture as a JointTrajectory
    def make_gripper_posture(self, joint_positions):
        # Initialize the joint trajectory for the gripper joints
        t = JointTrajectory()

        # Set the joint names to the gripper joint names
        t.joint_names = GRIPPER_JOINT_NAMES

        # Initialize a joint trajectory point to represent the goal
        tp = JointTrajectoryPoint()

        # Assign the trajectory joint positions to the input positions
        tp.positions = joint_positions

        # Set the gripper effort
        #tp.effort = GRIPPER_EFFORT

        tp.time_from_start = rospy.Duration(5)

        # Append the goal point to the trajectory points
        t.points.append(tp)

        # Return the joint trajectory
        return t

    # Generate a gripper translation in the direction given by vector
    def make_gripper_translation(self, min_dist, desired, vector):
        # Initialize the gripper translation object
        g = GripperTranslation()

        # Set the direction vector components to the input
        g.direction.vector.x = vector[0]
        g.direction.vector.y = vector[1]
        g.direction.vector.z = vector[2]

        # The vector is relative to the gripper frame
        g.direction.header.frame_id = REFERENCE_FRAME

        # Assign the min and desired distances from the input
        g.min_distance = min_dist
        g.desired_distance = desired

        return g

    # Generate a list of possible grasps
    def make_grasps(self, initial_pose_stamped, allowed_touch_objects):
        # Initialize the grasp object
        g = Grasp()

        # Set the pre-grasp and grasp postures appropriately
        g.pre_grasp_posture = self.make_gripper_posture(GRIPPER_OPEN)
        g.grasp_posture = self.make_gripper_posture(GRIPPER_GRASP)

        # 设置靠近抓取点的方向为沿着Z轴负向，最小移动距离0.08m，期望距离0.1米
        g.pre_grasp_approach = self.make_gripper_translation(0.08, 0.1, [0, 0.0,-1.0])

        # TODO：设置post_grasp_retreat：抓取后撤离的方向为沿着Z轴正向，最小移动距离0.08m，期望距离0.1米



        # 设置grasp_pose等于initial_pose_stamped，即被抓取对象的中心位置
        g.grasp_pose = initial_pose_stamped

        # Pitch angles to try
        pitch_vals = [0,0.03,0.05,-0.03,-0.05]

        # A list to hold the grasps
        grasps = []

        # Generate a grasp for each pitch and yaw angle
        for p in pitch_vals:
            # Create a quaternion from the Euler angles
            q = quaternion_from_euler(0, math.pi/2+p, 0)

            # Set the grasp pose orientation accordingly
            g.grasp_pose.pose.orientation.x = q[0]
            g.grasp_pose.pose.orientation.y = q[1]
            g.grasp_pose.pose.orientation.z = q[2]
            g.grasp_pose.pose.orientation.w = q[3]

            # Set and id for this grasp (simply needs to be unique)
            g.id = str(len(grasps))

            # Set the allowed touch objects to the input list
            g.allowed_touch_objects = allowed_touch_objects

            # Don't restrict contact force
            g.max_contact_force = 0

            # Append the grasp to the list
            grasps.append(deepcopy(g))

        # Return the list
        return grasps

    # Generate a list of possible place poses
    def make_places(self, init_pose):
        # Initialize the place location as a PlaceLocation message
        place = PlaceLocation()
        # Start with the input place pose
        place.place_pose = init_pose

        # TODO：设置pre_place_approach：靠近放置点的方向为Z轴负向、最小移动距离0.08m、期望距0.1m




        # 设置放置完成后机械臂的撤离方向、移动最小距离和期望距离，这里设置为沿着Z轴向上移动0.08米
        place.post_place_retreat.direction.header.frame_id = REFERENCE_FRAME;
        place.post_place_retreat.direction.vector.z = 1.0;
        place.post_place_retreat.min_distance = 0.04;
        place.post_place_retreat.desired_distance = 0.08;

        # x坐标的偏移量，放置时，允许机械臂放置在放置点x坐标稍微偏移的位置
        x_vals = [0, 0.005, 0.01,-0.005, -0.01]
        places = []
        # Generate a place pose for each angle and translation
        for x in x_vals:
            place.place_pose.pose.position.x = init_pose.pose.position.x + x
            # Append this place pose to the list
            places.append(deepcopy(place))
        # Return the list
        return places

if __name__ == "__main__":
    MoveItPickPlaceDemo()
