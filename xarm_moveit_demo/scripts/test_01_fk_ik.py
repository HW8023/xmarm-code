#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, sys
from math import pi
from geometry_msgs.msg import PoseStamped, Pose
import tf
from tf.transformations import *
import moveit_commander

class MoveItIkFKDemo:
    def __init__(self):

        # 初始化Python API 依赖的moveit_commanderC++系统，需放在前面
        moveit_commander.roscpp_initialize(sys.argv)

        # 初始化ROS节点，节点名为test_01_fk_ik
        rospy.init_node('moveit_ik_demo')

        # 把arm连接到规划组xarm
        arm = moveit_commander.MoveGroupCommander('xarm')

        # TODO：获取xarm规划组末端执行器link，保存到end_effector_link变量中


        # 设置目标位置所使用的参考坐标系为base_link
        reference_frame = 'base_link'
        arm.set_pose_reference_frame(reference_frame)

        # TODO：当运动规划失败后，允许重新规划


        # 设置位置(单位：米)和姿态（单位：弧度）的允许误差
        arm.set_goal_position_tolerance(0.02)
        arm.set_goal_orientation_tolerance(0.03)

        # TODO：设置末端执行器的目标位姿target_pose，参考坐标系为base_link
        #       target_pose的位置坐标xyz为（0.3，-0.3, 0.3）
        #       target_pose的姿态用RPY欧拉角表示为（0,0,-pi/4）
        #       Tips:可通过tf.transformations.quaternion_from_euler函数将RPY欧拉角转化为四元数







        # TODO：设置机器臂当前的状态作为运动初始状态


        # TODO：设置机械臂终端运动的目标位姿为上面设置的target_pose


        # TODO：进行轨迹规划并执行轨迹让机械臂运动到target_pose


        rospy.sleep(1)

        # 设置机械臂的目标位姿target_joint_positions，使用xarm组六个关节的位置数据进行描述（单位：弧度），每个位置对应着一个关节。
        target_joint_positions = [-0.9, -1.0, 0.2, 0.9, -0.76, 1.5]

        # TODO：使用set_joint_value_target()函数设置机械臂的目标为joint_positions



        # TODO：进行轨迹规划并执行轨迹让机械臂运动到joint_positions



        rospy.sleep(1)

        # TODO:使用set_named_target()函数设置目标为“Home”并控制机械臂回到初始化位置




        # 干净地关闭moveit_commander并退出程序
        moveit_commander.roscpp_shutdown()
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    MoveItIkFKDemo()
